#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>


struct HashTable {
    int key;
    int data;
};

struct Account {
    int account_key;
    int cash_balance;
    int option_share_balance;
    int vested_share_balace;
    struct TransactionHistory * transaction_history;
};

struct Transaction {
    int shares;
    char buyer;
    char seller;
    int sell_value;
    int buy_value;
};

struct TransactionHistory {
    struct Transaction * transaction;
    struct TransactionHistory * link;
};

struct Account * open_account(struct Account * account, struct HashTable * accounts, int balance, int share_options, int vested_shares, struct TransactionHistory * transactions);
bool findAccount(int accountKey, struct HashTable * accounts);

bool findAccount(int accountKey, struct HashTable * accounts) {
    return true;
}

struct Account * open_account(struct Account * account, struct HashTable * accounts, int balance, int share_options, int vested_shares, struct TransactionHistory * transactions) {
    bool accountExists = findAccount(account->account_key, accounts);
}

main() {
    struct Account * account;
    account=(struct Accout *)malloc(sizeof(struct Account));

    struct HashTable * accounts;
    accounts=(struct HashTable *)malloc(sizeof(struct HashTable));
}