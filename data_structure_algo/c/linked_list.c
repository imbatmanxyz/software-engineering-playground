#include<stdio.h>
#include<stdlib.h>


struct node {
    int info;
    struct node *link;
};

struct node *createList(struct node *start, int size);
struct node *insertAtBeginning(struct node *start, int data);
struct node *insertAtIndex(struct node *start, int index, int data);
struct node *insertBefore(struct node *start, int data, int value);
struct node *insertAfter(struct node *start, int data, int value);
struct node *insertAtEnd(struct node *start, int data);

void displayList(struct node *start);
void countNodes(struct node *start);
void search(struct node *start, int x);
void retrieveLastNode(struct node *start);
void predecessorNode(struct node *start);
struct node *positionNode(struct node *start, int pos);
struct node *deleteNode(struct node *start, int data);
struct node *reverseList(struct node *start);
struct node *allocateNodeMemory();


void main() {
    struct node *start=NULL;
    int size=5;

    start = createList(start, size);

    displayList(start);
    countNodes(start);
    search(start, 4);
    retrieveLastNode(start);
    predecessorNode(start);
    positionNode(start, 4);
    printf("Insert at the end of the single linked list: %d \n", 6);
    insertAtEnd(start, 6);
    displayList(start);
    printf("Insert %d into the single linked list after value: %d \n", 4, 2);
    insertAfter(start, 4, 2);
    displayList(start);
    printf("Insert %d into the single linked list before value: %d \n", 11, 4);
    insertBefore(start, 11, 4);
    displayList(start);
    printf("Insert at index %d of the single linked list: %d \n", 3, 8);
    insertAtIndex(start, 3, 8);
    displayList(start);
    deleteNode(start, 8);
    displayList(start);
    reverseList(start);
    displayList(start);
}

struct node *allocateNodeMemory() {
    return (struct node *)malloc(sizeof(struct node));
}

struct node *deleteNode(struct node *start, int data) {
    struct node *p, *temp;
    p=start;

    while(p->link!=NULL) {
        if(p->link->info==data) {
            temp=p->link;
            p->link=temp->link;
            free(temp);
            break;
        }

        p=p->link;
    }
}

struct node *reverseList(struct node *start) {
    struct node *prev, *ptr, *next;
    prev=NULL;
    ptr=start;

    while(ptr!=NULL) {
        next=ptr->link;
        ptr->link=prev;
        prev=ptr;
        ptr=next;
    }

    start=prev;
    return start;
}

struct node *insertBefore(struct node *start, int data, int value) {
    struct node *p, *temp;

    if (value==start->info) {
        temp=(struct node *)malloc(sizeof(struct node));
        temp->info=data;
        temp->link=start;
        start=temp;
        return start;
    }

    p=start;
    while(p->link!=NULL) {
        if(p->link->info==value) break;
        p=p->link;
    }

    if (p->link==NULL) printf("%d is not present in linked list \n", value);
    else {
        temp=(struct node *)malloc(sizeof(struct node));
        temp->info=data;
        temp->link=p->link;
        p->link=temp;
    }

    return start;
}

struct node *insertAfter(struct node *start, int data, int value) {
    struct node *p, *temp;

    p=start;
    while(p!=NULL) {
        if(p->info==value) break;
        p=p->link;
    }

    if (p==NULL) printf("%d is not present in list \n", value);
    else {
        temp=(struct node *)malloc(sizeof(struct node));
        temp->info=data;
        temp->link=p->link;
        p->link=temp;
    }

    return p;
}

struct node *insertAtIndex(struct node *start, int index, int data) {
    if(start==NULL) {
        printf("linked list is empty");
        return start;
    }

    struct node *p, *temp;
    p=positionNode(start, index);

    temp=allocateNodeMemory();
    temp->info=data;
    temp->link=p->link;
    p->link=temp;

    return p;
}

void retrieveLastNode(struct node *start) {
    struct node *p;
    p=start;

    while(p->link!=NULL) {
        p=p->link;
    }
    
    printf("last node contains info: %d \n", p->info);
}

void predecessorNode(struct node *start) {
    struct node *p;

    p=start;

    while(p->link->link!=NULL) {
        p=p->link;
    }

    printf("The predecessor node holds %d value \n", p->info);
}

struct node *positionNode(struct node *start, int pos) {
    int i;
    struct node *p;

    p=start;

    for(i=1; i<pos && p!=NULL; i++) {
        p=p->link;
    }

    printf("the value of node at position %d is equal to %d \n", pos, p->info);
    return p;
}

void search(struct node *start, int x) {
    if(start==NULL) {
        printf("List in empty");
        return;
    }

    printf("Searching for value %d in Linked List\n", x);

    while(start!=NULL) {
        if(start->info == x) {
            printf("Found in Linked List \n");
            break;
        }

        start=start->link;
    }
}

struct node *createList(struct node *start, int size) {
    if (size==0) return start;
    
    int data;

    data=1;
    start=insertAtBeginning(start, data);

    for(int i=2; i<=size; i++) insertAtEnd(start, i);

    return start;
}

void displayList(struct node *start) {
    struct node *p;

    if (start==NULL) {
        printf("List is empty");
        return;
    }

    printf("Linked List contains: ");

    p=start;
    while(p!=NULL) {
        printf("%d ", p->info);
        p=p->link;
    }

    printf("\n");
}

void countNodes(struct node *start) {
    int count;

    while(start!=NULL) {
        count++;
        start=start->link;
    }

    printf("Total count of nodes in Linked List: %d \n", count);
}

struct node *insertAtBeginning(struct node *start, int data) {
    struct node *temp;

    temp=(struct node *)malloc(sizeof(struct node));
    temp->info=data;
    temp->link=start;

    start=temp;
    return start;
}

struct node *insertAtEnd(struct node *start, int data) {
    struct node *temp;
    
    temp=(struct node *)malloc(sizeof(struct node));
    temp->info=data;
    temp->link=NULL;

    while(start->link!=NULL) {
        start=start->link;
    }

    start->link=temp;
    return start;
}