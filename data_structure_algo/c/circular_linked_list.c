#include <stdio.h>
#include <stdlib.h>


struct node
{
    int info;
    struct node * link;
};

struct node * insertInBeginning(struct node * last, int data);
struct node * insertInEmptyList(struct node * last, int data);
struct node * insertAtEnd(struct node * last, int data);

struct node * insertInBeginning(struct node * last, int data) {
    struct node * temp;
    temp=(struct node *)malloc(sizeof(struct node));
    temp->info=data;
    temp->link=last->link;
    last->link=temp;
    return last;
}

struct node * insertInEmptyList(struct node * last, int data) {
    struct node * temp;
    temp=(struct node *)malloc(sizeof(struct node));
    temp->info=data;
    last=temp;
    last->link=temp;
    return last;
}

struct node * insertAtEnd(struct node * last, int data) {
    struct node * temp;
    temp=(struct node *)malloc(sizeof(struct node));
    temp->info=data;
    temp->link=last->link;
    last->link=temp;
    last=temp;
    return last;   
}

main() {
    struct node * last=NULL;
}

struct node * createList(struct node * last, int data) {
    
}

void displayList(struct node * last) {
    struct node * p;
    if (last==NULL) {
        printf("empty list");
        return;
    }

    p=last->link;
    do {
        printf("%d", p->info);
        p=p->link;
    } while (p!=last->link);

    printf("\n");
}