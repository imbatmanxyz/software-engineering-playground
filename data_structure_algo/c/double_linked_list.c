#include <stdio.h>
#include <stdlib.h>


struct node {
    int info;
    struct node *prev;
    struct node *next;
};

struct node *shift(struct node *start, int data);
struct node *push(struct node *start, int data);
struct node *drop(struct node *start);
struct node *pop(struct node *start);
struct node *find(struct node *start, int data);
struct node *insert(struct node *start, int data, int position);

void repl(struct node *start);

struct node *insert(struct node *start, int data, int position) {
    struct node *p, *temp;
    temp=(struct node *)malloc(sizeof(struct node));

    p=start;
    if(start==NULL) {
        temp->info=data;
        temp->next=NULL;
        temp->prev=NULL;
        p=temp;

    }
    return p;
}

struct node *push(struct node *start, int data) {
    struct node *p, *temp;

    if(start==NULL) {
        p=insert(start, data, 0);
        return p;
    }

    temp=(struct node *)malloc(sizeof(struct node));
    temp->info=data;

    p=start;
    while(p->next!=NULL) {
        p=p->next;
    }

    p->next=temp;
    temp->next=NULL;
    temp->prev=p;
    
    return p;
}

struct node *shift(struct node *start, int data) {
    struct node *temp;

    if(start==NULL) {
        start=insert(start, data, 0);
        return start;
    }

    temp=(struct node *)malloc(sizeof(struct node));

    start=start;
    temp->info=data;
    temp->prev=NULL;
    temp->next=start;
    start->prev=temp;
    start=temp;

    return start;
}

void repl(struct node *start) {
    if (start==NULL) {
        printf("List is empty");
        return;
    }
    
    struct node *p;
    p=start;

    while(p!=NULL) {
        printf("%d", p->info);
        p=p->next;
    }

    printf("\n");
}

void main() {
    struct node *start;
    start=NULL;

    int dbl_linked_list_size=10;
    
    start=shift(start, 0);

    for(int data=1; data<dbl_linked_list_size; data++) {
        push(start, data);
    }

    repl(start);
}