# software-engineering-playground

A playground where I explore various software engineering practices, principles, languages, technologies, etc.

Data Structures and Algorithms
- C language
    - [ ] Linked List
    - [ ] Double Linked List
    - [ ] Stack and Queue
    - [ ] Recursion
    - [ ] Binary Tree
    - [ ] Binary Search Tree
    - [ ] Heap
    - [ ] Sorting
    - [ ] Searching
    - [ ] Hashing